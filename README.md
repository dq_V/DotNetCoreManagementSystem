# DotNetCore权限管理系统

#### 介绍
使用 asp.netcore3.1 mvc+layui+ef.core+mysql构建的权限管理系统，实现了基于用户角色的权限管理功能。权限、用户、角色部分的增删改查等。

#### 软件架构
多层架构，数据库支持mysql和sqlserver
包含数据库脚本
更改appsetting.json的数据库连接串信息为自己的。
适合初学者，学生看。
登录：/Login/Index 输入1就登录了，因为没做登录功能
![首页](managementSystem1.png)
![权限树列表](managementSystem2.png)
![角色管理](managementSystem3.png)
![用户管理](managementSystem4.png)
